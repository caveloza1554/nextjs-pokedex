import { notFound } from "next/navigation";

async function GetPokeInfo(pokemonId) {
  const pokemonInfo = {};
  try {
    const resSpecies = await fetch(
      `https://pokeapi.co/api/v2/pokemon-species/${pokemonId}/`
    );
    const resPokemon = await fetch(
      `https://pokeapi.co/api/v2/pokemon/${pokemonId}/`
    );

    const dataSpecies = await resSpecies.json();
    const dataPokemon = await resPokemon.json();

    pokemonInfo["stats"] = dataPokemon["stats"];
    pokemonInfo["height"] = dataPokemon["height"];
    pokemonInfo["weight"] = dataPokemon["weight"];

    pokemonInfo["types"] = dataPokemon["types"].map((e) => {
      return e.type.name;
    });

    pokemonInfo["id"] = dataSpecies["id"];
    pokemonInfo["name"] = dataSpecies["names"]
      .filter((entries) => {
        return entries.language.name == "es";
      })
      .map((text) => {
        return text.name;
      })[0];

    pokemonInfo["descriptions"] = dataSpecies["flavor_text_entries"]
      .filter((entries) => {
        return entries.language.name == "es";
      })
      .map((text) => {
        return { description: text.flavor_text, version: text.version.name };
      });

    pokemonInfo["category"] = dataSpecies["genera"]
      .filter((entries) => {
        return entries.language.name == "es";
      })
      .map((text) => {
        return text.genus;
      })[0];
    pokemonInfo["ability"] = dataPokemon["abilities"].filter((ab) => {
      return ab.is_hidden == false;
    })[0];

    return pokemonInfo;
  } catch (e) {
    return {
      notFound: true,
    };
  }
}

async function PokemonInfo({ params }) {
  const pokemonId = params.pokemonId;

  const pokemonInfo = await GetPokeInfo(pokemonId);
  console.log(pokemonInfo);
  if (pokemonInfo.notFound) {
    notFound();
  }

  return (
    <div className="flex flex-col justify-center items-center">
      <h1>
        {pokemonInfo.name} N.º{pokemonInfo.id.toString().padStart(4, "0")}
      </h1>
      {/* <img src="{pokemonInfo.}" alt="" srcset="" /> */}
      <p>{pokemonInfo.descriptions[0].description}</p>
      <div>
        {pokemonInfo.stats.map((stat) => {
          return (
            <h1>
              {" "}
              {stat.stat.name}: {stat.base_stat}
            </h1>
          );
        })}
      </div>
      <div>
        <span>Altura {pokemonInfo.height}</span>
        <span>Categoría {pokemonInfo.category}</span>
        <span>Peso {pokemonInfo.weight}</span>
        <span>Habilidad {pokemonInfo.ability.ability.name}</span>
        <div>
          <span>Tipo</span>
          {pokemonInfo.types.map((type) => {
            return <span>{type}</span>;
          })}
        </div>
      </div>
    </div>
  );
}

export default PokemonInfo;
