import { PokedexHeader } from "./components/pokedexHeader.jsx";
import { PokedexSearch } from "./components/pokedexSearch.jsx";
import PokemonList from "./components/scrollPokemonCards.jsx";

function Home() {
  return (
    <div className="flex flex-col justify-center items-center">
      <PokedexHeader />
      <PokedexSearch />

      <section className="max-w-screen-lg w-full mt-10 block float-left relative mb-[50px] mx-[0.78125%]">
        <PokemonList />
      </section>
    </div>
  );
}

export default Home;
