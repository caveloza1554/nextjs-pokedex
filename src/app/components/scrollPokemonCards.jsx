"use client";

import { useState, useEffect } from "react";
import { PokemonCard } from "./pokemonCard";

const PokemonList = () => {
  const [pokemonList, setPokemonList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [offset, setOffset] = useState(0);

  const fetchPokemon = async () => {
    const url = `https://pokeapi.co/api/v2/pokemon/?offset=${offset}&limit=20`;
    try {
      setLoading(true);
      console.log("url: ", url);
      const response = await fetch(url);
      const data = await response.json();
      setPokemonList((prevList) => [...prevList, ...data.results]);
    } catch (error) {
      console.error("Error fetching Pokémon:", error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchPokemon();
  }, [offset]);

  useEffect(() => {
    const handleScroll = () => {
      if (
        window.innerHeight + document.documentElement.scrollTop >=
        document.documentElement.offsetHeight
      ) {
        setOffset((prevOffset) => prevOffset + 20);
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <section className="max-w-screen-lg w-full mt-10 block float-left relative mb-[50px] mx-[0.78125%]">
      <ul className="grid grid-cols-4 gap-4">
        {pokemonList.map(({ name, url }) => {
          return (
            <li key={name}>
              <PokemonCard pokemonUrl={url}></PokemonCard>
            </li>
          );
        })}
      </ul>
      {loading && <p>Cargando...</p>}
    </section>
  );
};

export default PokemonList;
