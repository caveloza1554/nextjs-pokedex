"use client";
import Link from "next/link";
import { useState, useEffect } from "react";

const types = {
  bug: { name: "Bicho", style: "background-color-bug" },
  dark: { name: "Siniestro", style: "background-color-dark" },
  dragon: { name: "Dragón", style: "background-color-dragon" },
  electric: { name: "Eléctrico", style: "background-color-electric" },
  fairy: { name: "Hada", style: "background-color-fairy" },
  fighting: { name: "Lucha", style: "background-color-fighting" },
  fire: { name: "Fuego", style: "background-color-fire" },
  flying: { name: "Volador", style: "background-color-flying" },
  ghost: { name: "Fantasma", style: "background-color-ghost" },
  grass: { name: "Planta", style: "background-color-grass" },
  ground: { name: "Roca", style: "background-color-ground" },
  ice: { name: "Hielo", style: "background-color-ice" },
  normal: { name: "Normal", style: "background-color-normal" },
  poison: { name: "Veneno", style: "background-color-poison" },
  psychic: { name: "Psíquico", style: "background-color-psychic" },
  rock: { name: "Roca", style: "background-color-rock" },
  steel: { name: "Acero", style: "background-color-steel" },
  water: { name: "Agua", style: "background-color-water" },
};

export function PokemonCard({ pokemonUrl }) {
  const [pokemonCard, setPokemonCard] = useState({});

  const fetchPokemonInformation = async () => {
    const res = await fetch(pokemonUrl);
    const data = await res.json();
    setPokemonCard(data);
  };

  useEffect(() => {
    fetchPokemonInformation();
  }, []);

  return (
    <div className="bg-white w-[205px] rounded-lg pb-5">
      <Link href={`/${pokemonCard?.id}`}>
        <img
          className="bg-slate-300 w-[205px] h-[205px] rounded-lg"
          src={pokemonCard?.sprites?.front_default}
          alt={pokemonCard?.name}
        />
      </Link>
      <div>
        <p className="font-bold text-[#919191] text-[80%] pt-0.5 pl-5">
          <span>N.º&nbsp;</span>
          {pokemonCard?.id}
        </p>
        <h5 className="capitalize text-[#313131] text-[145%] mb-[5px] pl-5">
          {pokemonCard?.name}
        </h5>
      </div>
      <div className="flex justify-between ml-5 mr-7">
        {pokemonCard?.types?.map(({ type }) => {
          const style = `${
            types[type.name].style
          } leading-[18px] max-w-[110px] w-[38.4375%] text-[11px] text-center mr-[1.5625%];`;
          return (
            <span key={type.name} className={style}>
              {types[type.name].name}
            </span>
          );
        })}
      </div>
    </div>
  );
}
