export function PokedexSearch() {
    return (
        <section className="max-w-screen-lg w-full mt-10 space-y-10 md:flex md:space-x-10">
            <div className="ml-12 w-96 space-y-5">
                <label className="text-[167.5%]">Nombre o número</label>
                <div className="space-x-5 flex w">

                    <input type="text" className="rounded-lg flex-1 relative align-top h-12 text-black p-3" />
                    <input type="submit" value=""
                        className="rounded-lg w-12 h-12 bg-[#ee6b2f] bg-[url(https://assets.pokemon.com/static2/\_ui/img/chrome/forms/input-search-bg.png)] bg-center bg-no-repeat">
                    </input>

                </div>
                
            </div>

            <div className="rounded-lg bg-[#4dad5b] h-24 ml-12 w-96 flex justify-center items-center text-center">
                <div className="">
                    <h2 >Busca un Pokémon por su nombre o usando su número de la Pokédex Nacional.</h2>
                </div>
            </div>
        </section>
    )
}

