export function PokedexHeader() {
    return (

        <section className="rounded-lg text-[#919191] bg-[#ffffff] max-w-screen-lg w-full pt-[25px] ;">
            <h1 className="text-[187.5%] ml-12 mb-3">
                Pokédex
            </h1>
        </section>

    )
}